/*

Officer: 8768770
CaseNum: 101-3-19753666-8768770

Case 101 - The Case of Lina Lovelace
Stage 4 - The Plaza Hotel

Okay this place is more Lina’s style. Now’s our chance to find out the root of all
of this. Lets see who is Lina meeting.

Identify Lina by drawing a blue filled rectangle with a cyan outline.
She’s the woman in the red dress of course.

Identify the man with the monocle smoking the cigar magenta filled
rectangle with a cyan outline around him.

Identify the man reading the newspaper by drawing a blue filled rectangle
with a cyan outline around him.

Identify the woman with the dog by drawing a yellow filled rectangle with a
blue outline around her. Make sure you include the dog too.

The rectangles should cover the targets as accurately as possible without
including anything else.

There are many possible ways of investigating this case, but you
should use ONLY the following commands:

  rect()
  fill() Use only 255 or 0 for r,g,b values. Set alpha to 100 for some opacity.
	stroke() Use only 255 or 0 for r,g,b values.

*/

var img;

function preload()
{
	img = loadImage('img.jpg');
}

function setup()
{
	createCanvas(img.width,img.height);
	strokeWeight(2);
}

function draw()
{
    
	//Write your code below here ...
	image(img,0,0);
    fill(0,0,255,100)
    stroke(0,255,255)
    rect(1067,278,1254-1067,655-278)
    
    fill(255,0,255,100)
    stroke(0,255,255)
    rect(493,343,591-493,475-343)
        
    fill(0,0,255,100)
    stroke(0,255,255)
    rect(14,299,210-14,684-299)
        
    fill(255,255,0,100)
    stroke(0,0,255)
    rect(806,260,960-806,586-260)


}
