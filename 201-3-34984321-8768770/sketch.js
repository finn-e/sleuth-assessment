/*
201 - The case of Judge Hopper
Stage 4 - The warehouse

Officer: 8768770
CaseNum: 201-3-34984321-8768770

As you enter the ALGOL warehouse you are struck by the most horrendous stench - it’s not the fish. Lying amongst piles of fish carcasses you find the body of Judge Hopper. Gathering yourself together, you tie a handkerchief around your nose and mouth and quickly set about recording the evidence.

Draw around the Judge’s body ...


*/

var img;

function preload()
{
    img = loadImage('scene.png');
}

function setup()
{
    createCanvas(img.width,img.height);
}

function draw()
{

    image(img,0,0);
    stroke(255, 0, 0);
    strokeWeight(3);
    noFill();

    // write the code to draw around the Judge's body below
    beginShape();
    vertex(439,343);
    vertex(471,357);
    vertex(504,326);
    vertex(527,467);
    vertex(758,530);
    vertex(758,476);
    vertex(646,404);
    vertex(820,153);
    vertex(700,60);
    vertex(553,316);
    vertex(515,286);
    vertex(439,343);
    endShape();
}
