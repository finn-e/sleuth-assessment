/*

Officer: 8768770
CaseNum: 202-0-12723417-8768770

Case 202 - The case of Bob and Daisy - stage 1

That pair of notorious criminals Woz and Jobs are up to no good again.
I’ve intercepted letters sent between them. It seems that they are
communicating through an ingenious code in which they masquerade as
besotted lovers, Daisy and Bob. I need you crack their code and determine
the details of their next heist so that we can catch them in the act.

Discover the hidden code by commenting out all text commands except
those which produce black text. Only comment out text commands.
Leave fill commands uncommented.

There are many possible ways of investigating this case, but you
should use ONLY the following commands:

  // comments are all that are needed for this case.
  Do NOT add any new lines of code.

*/

var letterFont;

function preload()
{
	letterFont = loadFont('Ballpointprint.ttf');
}

function setup()
{
	createCanvas(616,620);
	textFont(letterFont);
	textSize(31);
}

function draw()
{
	background(255);

	fill(0,0,0);
	text("date", 253,231);
	text("April", 457,369);
	fill(0,255,0);
//	text("your", 457,197);
//	text("I", 77,93);
//	text("confession", 198,93);
	fill(0,0,255);
//	text("last", 203,231);
	fill(255,0,0);
//	text("Oh", 17,31);
//	text("x", 77,493);
//	text("few", 158,331);
	fill(0,0,0);
	text("a", 170,93);
	text("day", 365,369);
	text("chosen", 514,197);
	fill(255,0,0);
//	text("since", 89,231);
//	text("our", 159,231);
//	text("be", 505,159);
//	text("s.", 295,262);
//	text("isy,", 174,31);
	fill(0,0,255);
//	text("I", 375,197);
//	text("You", 143,369);
	fill(255,0,255);
//	text("of", 531,231);
//	text("when", 435,93);
//	text("one", 541,331);
	fill(0,0,255);
//	text("the", 393,262);
//	text("lovely", 64,31);
	fill(0,255,0);
//	text("s", 292,331);
//	text("make", 115,93);
//	text("saw", 211,296);
//	text("Love", 17,431);
//	text("lovely", 319,296);
//	text("true", 21,369);
	fill(0,0,255);
//	text("were", 424,331);
//	text("first", 447,262);
	fill(0,0,0);
	text("second", 221,331);
	text("in", 422,369);
	fill(0,0,255);
//	text("I", 173,296);
//	text("luckiest", 77,197);
//	text("can", 359,231);
	fill(0,255,0);
//	text("I", 308,127);
	fill(255,0,0);
//	text("the", 289,159);
//	text("only", 408,231);
//	text("From", 326,262);
	fill(255,0,255);
//	text("the", 23,197);
	fill(0,255,0);
//	text("qui", 177,127);
	fill(0,0,255);
//	text("kisses,", 139,431);
	fill(255,0,255);
//	text("blessed", 75,159);
//	text("from", 20,331);
//	text("am", 544,93);
//	text("the", 123,127);
//	text("the", 21,159);
	fill(0,0,255);
//	text("that", 318,331);
	fill(255,0,255);
//	text("Bob", 17,493);
//	text("voice", 468,127);
	fill(0,255,0);
//	text("h", 343,159);
//	text("that", 116,296);
//	text("Ever", 19,231);
	fill(0,0,255);
//	text("and", 84,431);
//	text("think", 463,231);
	fill(255,0,0);
//	text("alive", 251,197);
//	text("eye", 250,262);
//	text("is", 402,93);
	fill(0,255,0);
//	text("that", 251,127);
	fill(0,0,255);
//	text("must", 445,159);
//	text("alone", 19,127);
//	text("arp.", 356,159);
//	text("et", 210,127);
	fill(255,0,0);
//	text("tho", 86,331);
//	text("Da", 140,31);
	fill(0,0,255);
//	text("in", 88,127);
//	text("I", 407,159);
//	text("your", 17,262);
	fill(255,0,255);
//	text("It", 356,93);
//	text("your", 262,296);
//	text("May", 17,93);
//	text("knew", 508,296);
//	text("your", 411,127);
	fill(0,255,0);
//	text("lo", 80,369);
//	text("I", 470,296);
	fill(0,0,255);
//	text("like", 538,127);
//	text("person", 171,197);
	fill(255,0,0);
//	text("that", 318,197);
//	text("my", 248,369);
//	text("you", 375,331);
//	text("moment", 23,296);
//	text("?", 322,93);
//	text("hear", 346,127);
	fill(0,255,0);
//	text("of", 247,159);
//	text("se", 116,331);
//	text("darling,", 74,262);
//	text("are", 196,369);
	fill(255,0,255);
//	text("am", 413,197);
//	text("green", 168,262);
//	text("I", 506,93);
//	text("sunny", 294,369);
//	text("music", 178,159);
//	text("face,", 395,296);
	fill(255,0,0);
//	text("my", 495,331);
//	text("I", 321,231);
//	text("ve.", 94,369);



}
