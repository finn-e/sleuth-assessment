/*

Officer: 8768770
CaseNum: 202-1-15499083-8768770

Case 202 - The case of Bob and Daisy - stage 2

Here’s another letter kid. This time it’s from Daisy (aka. Woz).
Decode it to uncover more about Woz and Job’s dastardly plan.

Discover the hidden code by commenting out all text commands except
those which produce yellow filled text with blue outline.
Only comment out text commands - leave fill & stroke commands uncommented.

There are many possible ways of investigating this case, but you
should use ONLY the following commands:

  // comments are all that are needed for this case.
  Do NOT add any new lines of code.

*/

var letterFont;

function preload()
{
	letterFont = loadFont('Melissa.otf');
}

function setup()
{
	createCanvas(518,364);
	textFont(letterFont);
	textSize(22);
}

function draw()
{
	background(255);

	fill(255,165,0);
	stroke(0,0,0);
//	text("swift", 210,163);
//	text("do", 377,141);
	stroke(255,0,0);
//	text("When", 114,115);
//	text("you.", 88,115);
//	text("you", 149,93);
	fill(255,255,0);
//	text("in", 173,93);
//	text("it", 384,66);
	fill(0,255,255);
//	text("this", 79,141);
//	text("How", 253,66);
	fill(255,192,203);
	stroke(0,255,0);
//	text("longer", 337,141);
//	text("to", 440,141);
	stroke(0,0,255);
//	text("I", 64,93);
//	text("I'm", 91,163);
//	text("only", 42,115);
	fill(255,255,0);
	stroke(0,255,0);
//	text("I", 247,93);
	fill(255,192,203);
	stroke(255,0,0);
//	text("is", 368,66);
//	text("the", 352,115);
	stroke(0,0,255);
//	text("I", 35,66);
	fill(0,255,255);
//	text("How", 273,141);
//	text("you", 474,115);
	fill(255,192,203);
	stroke(255,0,255);
//	text("last", 459,66);
//	text("in", 338,115);
	stroke(0,255,0);
//	text("ain", 272,115);
	fill(255,165,0);
//	text("banking", 115,163);
//	text("at", 8,141);
//	text("months", 318,66);
	fill(255,192,203);
	stroke(255,0,255);
//	text("we", 176,115);
	fill(0,255,255);
	stroke(255,0,0);
//	text("longing", 36,163);
//	text("feels", 178,141);
//	text("and", 427,93);
	fill(255,165,0);
//	text("I", 452,93);
	stroke(0,0,255);
//	text("desolate.", 214,141);
//	text("I", 396,141);
	stroke(255,0,255);
//	text("be", 200,115);
//	text("Bob,", 72,22);
//	text("we", 435,66);
	stroke(0,0,0);
//	text("yours,", 57,207);
//	text("return.", 252,163);
//	text("arms.", 209,93);
	fill(0,255,255);
//	text("this", 6,163);
//	text("around", 146,66);
	stroke(255,0,255);
//	text("the", 189,66);
//	text("your", 180,163);
//	text("sm", 109,141);
	fill(255,192,203);
	stroke(0,0,0);
//	text("think", 7,115);
//	text("of", 69,115);
//	text("?", 79,163);
//	text("my", 27,141);
//	text("night", 361,93);
//	text("having", 80,66);
//	text("place.", 215,66);
	fill(0,255,255);
//	text("?", 52,93);
//	text("My", 5,22);
//	text("sky,", 396,93);
//	text("without", 421,115);
	stroke(255,0,255);
//	text("ag", 258,115);
//	text("can", 463,93);
//	text("tare", 266,93);
	fill(255,255,0);
//	text("since", 399,66);
//	text("will", 152,115);
//	text("to", 102,93);
	stroke(255,0,0);
//	text("x", 44,251);
	fill(255,165,0);
	stroke(0,0,0);
//	text("kissed", 7,93);
	stroke(255,0,255);
//	text("s", 258,93);
	fill(0,255,255);
	stroke(0,255,0);
//	text("miss", 46,66);
//	text("have", 407,141);
//	text("united", 218,115);
	fill(255,165,0);
	stroke(255,0,255);
//	text("on", 162,163);
//	text("?", 293,115);
	fill(255,192,203);
	stroke(0,0,0);
//	text("all", 127,141);
	fill(0,255,255);
	stroke(255,0,255);
//	text("How", 5,66);
//	text("long", 75,93);
	fill(255,255,0);
//	text("the", 335,93);
	stroke(255,0,0);
//	text("town", 143,141);
//	text("darling", 29,22);
//	text("you", 122,66);
	stroke(0,0,0);
//	text("many", 283,66);
//	text("Forever", 5,207);
//	text("Even", 305,115);
	stroke(0,0,255);
	text("side", 49,141);
	text("up", 298,93);
	text("spring", 378,115);
	text("at", 316,93);
	text("hold", 121,93);
	text("store", 459,141);
	stroke(255,0,255);
//	text("much", 303,141);
//	text("Daisy", 5,251);
	stroke(0,255,0);
//	text("my", 187,93);



}
