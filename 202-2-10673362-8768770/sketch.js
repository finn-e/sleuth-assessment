/*

Officer: 8768770
CaseNum: 202-2-10673362-8768770

Case 202 - The case of Bob and Daisy - stage 3

Wow these two like to ham it up. Here’s the next letter. This time it’s from Bob (aka. Jobs).
I need you to decode it to uncover more details about their plan.

Discover the hidden code by commenting out all text commands except
those which produce magenta filled text in Ballpointprint font.
Only comment out text commands - leave fill & stroke commands uncommented.

There are many possible ways of investigating this case, but you
should use ONLY the following commands:

  // comments are all that are needed for this case.
  Do NOT add any new lines of code.

*/

var Ballpointprint;
var Melissa;
var Diggity;
var RonsFont;


function preload()
{
	Ballpointprint = loadFont('Ballpointprint.ttf');
	Melissa = loadFont('Melissa.otf');
	Diggity = loadFont('Diggity.ttf');
	RonsFont = loadFont('RonsFont.ttf');
}

function setup()
{
	createCanvas(562,506);
	textSize(26);
}

function draw()
{
	background(255);

	fill(0,255,0);
	textFont(RonsFont);
//	text("il", 435,78);
	textFont(Diggity);
//	text("old", 246,194);
//	text("ir", 483,194);
	fill(255,0,0);
//	text("all", 43,166);
//	text("be", 138,224);
//	text("I", 385,256);
	fill(0,255,0);
	textFont(Melissa);
//	text("and", 62,341);
//	text("to", 524,256);
	fill(0,0,255);
	textFont(RonsFont);
//	text("woods", 10,194);
//	text("raising", 168,224);
	textFont(Diggity);
//	text("the", 208,194);
	fill(255,0,0);
//	text("to", 14,166);
	textFont(RonsFont);
//	text("all", 371,106);
//	text("long", 430,106);
//	text("broadcast", 314,135);
	textFont(Melissa);
//	text("kiss", 91,341);
	fill(255,0,255);
//	text("Oh", 6,26);
//	text("I", 455,78);
//	text("from", 50,106);
	textFont(Ballpointprint);
	text("gun", 139,78);
	fill(0,0,0);
//	text("lovely", 34,26);
	fill(255,0,0);
	textFont(Melissa);
//	text("we", 324,106);
//	text("the", 457,166);
	fill(255,0,255);
	textFont(Ballpointprint);
	text("the", 267,256);
	fill(0,0,0);
	textFont(RonsFont);
//	text("sh", 351,106);
	fill(255,0,0);
//	text("unt", 397,78);
//	text("at", 68,166);
	textFont(Melissa);
//	text("es,", 120,341);
	fill(0,0,255);
	textFont(RonsFont);
//	text("Jerrys", 334,224);
	fill(0,255,0);
//	text("return", 232,106);
//	text("counting", 182,78);
	textFont(Diggity);
//	text("the", 320,78);
//	text("my", 97,289);
	textFont(Ballpointprint);
//	text("Love", 6,341);
	textFont(RonsFont);
//	text("to", 372,289);
	fill(255,0,0);
	textFont(Ballpointprint);
//	text("my", 194,106);
//	text("Bob", 6,393);
//	text("x", 57,393);
	textFont(Melissa);
//	text("back", 335,289);
	fill(0,255,0);
	textFont(Diggity);
//	text("and", 11,256);
	fill(0,0,255);
//	text("car", 131,289);
//	text("I", 263,166);
	fill(255,0,0);
	textFont(Ballpointprint);
//	text("our", 429,135);
//	text("by", 170,194);
	fill(0,0,0);
//	text("keep", 18,135);
//	text("Do", 358,194);
//	text("not", 399,194);
	fill(255,0,255);
	text("at", 304,224);
	textFont(Diggity);
//	text("now", 210,289);
//	text("no", 402,106);
	fill(255,0,0);
//	text("in", 75,289);
	fill(0,0,0);
//	text("will", 104,224);
	textFont(Melissa);
//	text("I", 9,78);
//	text("devo", 466,135);
//	text("days", 358,78);
	textFont(Ballpointprint);
//	text("walks", 384,166);
	fill(0,255,0);
	textFont(Melissa);
//	text("Upon", 153,106);
//	text("tempted", 457,256);
	fill(0,0,255);
	textFont(Ballpointprint);
//	text("am", 398,256);
	textFont(Diggity);
//	text("despa", 437,194);
//	text("our", 76,135);
	textFont(RonsFont);
//	text("Daisy,", 97,26);
//	text("bar", 466,224);
//	text("we", 65,224);
	fill(255,0,0);
	textFont(Ballpointprint);
//	text("love", 115,135);
	fill(0,0,0);
	textFont(Melissa);
//	text("tion", 498,135);
//	text("shall", 277,135);
//	text("of", 325,166);
//	text("down", 280,78);
	textFont(Diggity);
//	text("opportunity.", 156,166);
//	text("playing", 50,256);
	fill(0,0,255);
	textFont(RonsFont);
//	text("er", 469,106);
//	text("run", 283,289);
//	text("have", 61,78);
//	text("!", 506,194);
	fill(255,0,0);
	textFont(Diggity);
//	text("shooters", 110,256);
	fill(0,0,255);
	textFont(Melissa);
//	text("and", 85,194);
	textFont(Diggity);
//	text("all", 91,106);
//	text("every", 100,166);
	fill(255,0,255);
	textFont(Ballpointprint);
	text("hidden", 163,135);
	textFont(Diggity);
//	text("Soon", 17,224);
	fill(0,0,0);
//	text("harbour.", 278,194);
	textFont(Melissa);
//	text("so", 434,256);
	fill(0,0,255);
//	text("away", 6,106);
	textFont(Diggity);
//	text("too", 23,78);
	textFont(Melissa);
//	text("get", 507,78);
//	text("you.", 406,289);
	fill(255,0,255);
	textFont(Ballpointprint);
	text("down", 114,194);
	text("arcade", 312,256);
	fill(0,0,0);
	textFont(Melissa);
//	text("dre", 276,166);
//	text("am", 299,166);
	fill(0,255,0);
	textFont(Ballpointprint);
//	text("be", 113,78);
	fill(0,0,255);
	textFont(Melissa);
//	text("right", 170,289);
//	text("at", 245,256);
//	text("and", 254,289);
	fill(0,0,0);
//	text("th", 116,106);
//	text("toasts", 248,224);
	fill(0,255,0);
	textFont(Ballpointprint);
//	text("jump", 19,289);
//	text("our", 347,166);
//	text("wine", 412,224);
	fill(0,0,255);
	textFont(Melissa);
//	text("is.", 132,106);
//	text("in", 441,166);
	fill(0,255,0);
	textFont(Diggity);
//	text("down", 191,256);
//	text("We", 242,135);
//	text("can", 469,78);



}
