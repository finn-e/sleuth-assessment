/*

Officer: 8768770
CaseNum: 202-3-44345927-8768770

Case 202 - The case of Bob and Daisy - stage 4

Here’s the final letter from Daisy (aka. Woz). Decode it to uncover the
final details about Woz and Job’s dastardly plan.

Discover the hidden code by commenting out all text commands except
those which produce yellow filled text with black outline in Ballpointprint font.
Only comment out text commands - leave fill & stroke, push and pop commands uncommented.

There are many possible ways of investigating this case, but you
should use ONLY the following commands:

  // comments are all that are needed for this case.
  Do NOT add any new lines of code.

*/

var letterFont;

function preload()
{
	Ballpointprint = loadFont('Ballpointprint.ttf');
	Melissa = loadFont('Melissa.otf');
	Diggity = loadFont('Diggity.ttf');
	RonsFont = loadFont('RonsFont.ttf');
}

function setup()
{
	createCanvas(583,540);
	textSize(31);
}

function draw()
{
	background(255);

	fill(255,165,0);
	stroke(255,0,0);
	textFont(Ballpointprint);
//	text("can", 455,264);
	stroke(0,255,0);
	textFont(RonsFont);
//	text("My", 7,31);
	stroke(255,0,0);
	textFont(Diggity);
//	text("reak", 330,230);
	stroke(0,0,0);
	textFont(Melissa);
//	text("and", 389,230);
	fill(0,255,255);
	stroke(0,255,0);
	textFont(Ballpointprint);
//	text("sure", 400,93);
	fill(255,255,0);
	stroke(255,0,0);
//	text("relationship", 217,296);
//	text("Bob,", 125,31);
	push();
	fill(0,255,255);
//	text("Forever", 13,358);
	pop();
	textFont(Diggity);
//	text("can", 196,127);
//	text("Is", 148,296);
	stroke(255,0,255);
	textFont(RonsFont);
//	text("you", 125,264);
	fill(255,192,203);
	textFont(Melissa);
//	text("ignore", 148,198);
	push();
	fill(255,165,0);
	textFont(RonsFont);
//	text("?", 287,127);
//	text("so", 186,163);
	pop();
	textFont(Diggity);
//	text("a", 294,230);
	push();
	fill(0,255,255);
	stroke(255,0,0);
	textFont(Ballpointprint);
//	text("Daisy", 11,420);
	pop();
	textFont(RonsFont);
//	text("all", 531,230);
	fill(255,165,0);
	textFont(Melissa);
//	text("are", 152,163);
	push();
	fill(0,255,255);
	textFont(RonsFont);
//	text("not", 347,93);
	pop();
//	text("?", 258,93);
	fill(255,192,203);
	stroke(0,0,255);
	textFont(RonsFont);
//	text("I", 459,163);
//	text("send", 17,296);
//	text("out.", 13,264);
	fill(255,165,0);
	stroke(0,255,0);
	textFont(Diggity);
//	text("short", 179,264);
	push();
	textFont(Ballpointprint);
//	text("no", 25,198);
	pop();
	stroke(0,0,0);
	textFont(Ballpointprint);
//	text("I'm", 275,93);
//	text("so,", 399,264);
	push();
	stroke(255,0,0);
	textFont(RonsFont);
//	text("we", 14,230);
	pop();
	stroke(0,0,255);
	textFont(RonsFont);
//	text("nger", 81,198);
	textFont(Melissa);
//	text("take", 242,127);
	push();
	fill(0,255,255);
	stroke(0,255,0);
	textFont(RonsFont);
//	text("yours,", 115,358);
	pop();
	stroke(255,0,255);
//	text("If", 374,264);
	push();
	fill(0,255,255);
	stroke(0,0,0);
	textFont(Ballpointprint);
//	text("Are", 17,93);
	pop();
	fill(255,255,0);
	textFont(Diggity);
//	text("should", 61,230);
	stroke(0,0,255);
	textFont(RonsFont);
//	text("?", 352,264);
//	text("x", 91,420);
	push();
	stroke(255,0,255);
//	text("lo", 62,198);
	pop();
//	text("continual", 272,198);
	stroke(255,0,0);
//	text("away", 170,230);
	push();
	fill(255,192,203);
	textFont(Ballpointprint);
//	text("our", 173,296);
	pop();
	stroke(0,255,0);
//	text("of", 245,264);
	push();
	fill(0,255,255);
	stroke(0,0,0);
	textFont(Diggity);
//	text("darling", 53,31);
	pop();
	textFont(Ballpointprint);
//	text("secrets,", 376,127);
//	text("ing", 178,93);
	stroke(255,0,255);
//	text("silence.", 19,163);
	fill(0,255,255);
//	text("safe", 348,296);
	fill(255,255,0);
	textFont(Melissa);
//	text("You", 115,163);
//	text("elays.", 407,198);
	stroke(0,0,255);
	textFont(Ballpointprint);
//	text("I", 158,127);
	fill(0,255,255);
	textFont(Diggity);
//	text("these", 205,198);
	fill(255,255,0);
	textFont(Ballpointprint);
//	text("can", 486,163);
	fill(255,192,203);
	textFont(Diggity);
//	text("Perhaps", 460,198);
	fill(0,255,255);
//	text("money", 282,264);
	fill(255,255,0);
	stroke(0,255,0);
	textFont(Ballpointprint);
//	text("sometimes.", 331,163);
	fill(255,192,203);
	textFont(RonsFont);
//	text("more", 80,127);
//	text("sort", 425,230);
	stroke(0,0,255);
	textFont(Ballpointprint);
//	text("how", 460,93);
	textFont(Melissa);
//	text("d", 397,198);
//	text("I", 439,264);
	fill(255,255,0);
	stroke(0,0,0);
	textFont(Ballpointprint);
	text("cash", 88,296);
	text("the", 474,127);
	text("you", 75,93);
	text("avoid", 124,93);
	text("for", 244,230);
	text("guard", 223,163);
	text("go", 130,230);
	push();
	fill(255,192,203);
	textFont(Diggity);
//	text("much", 18,127);
//	text("?", 417,296);
	pop();
	fill(0,255,255);
	stroke(0,255,0);
	textFont(Diggity);
//	text("b", 319,230);
	stroke(0,0,255);
	textFont(Ballpointprint);
//	text("ed", 282,163);
	fill(255,192,203);
	stroke(0,255,0);
//	text("The", 309,127);
//	text("Are", 67,264);
	fill(0,255,255);
	textFont(Melissa);
//	text("this", 490,230);
	fill(255,192,203);
	stroke(255,0,0);
//	text("me", 227,93);



}
