/*
The case of the Python Syndicate
Stage 1

Officer: 8768770
CaseNum: 301-0-24941249-8768770

I gotta give it to you kid, you’ve made an excellent start, but now it’s time
to take things up a level. For some time I’ve suspected that there’s something
big going down in Console City.

These cases that we’ve been working are all connected somehow. I need to use
that considerable brain of yours to work it all out. Let’s start by laying out
who we know.

Place each mugshot in its designated position by doing the following:

- Create a new variable for the X and Y coordinates of each mugshot.
    - One has already been done for you.
    - Make sure you use the same style and format for the variable name.
- Find coordinates for the mugshot and initialise your variable with these
values.
- Replace the hard-coded constants in the corresponding image command so that
the mugshot appears in its designated position.

*/

var photoBoard;
var cecilKarpinskiImage;
var annaKarpinskiImage;
var rockyKrayImage;
var pawelKarpinskiImage;
var countessHamiltonImage;
var robbieKrayImage;



//declare your new variables below
var annaKarpinskiCoordX = 408;
var annaKarpinskiCoordY = 40;
var photoBoardCoordX = 0;
var photoBoardCoordY = 0;
var cecilKarpinskiCoordX = 115;
var cecilKarpinskiCoordY = 40;
var rockyKrayCoordX = 701;
var rockyKrayCoordY = 40;
var pawelKarpinskiCoordX = 115;
var pawelKarpinskiCoordY = 309;
var countessHamiltonCoordX = 408;
var countessHamiltonCoordY = 309;
var robbieKrayCoordX = 701;
var robbieKrayCoordY = 309;

function preload()
{
	photoBoard = loadImage('photoBoard.png');
	cecilKarpinskiImage = loadImage("karpinskiBros1.png");
	annaKarpinskiImage = loadImage("karpinskiWoman.png");
	rockyKrayImage = loadImage("krayBrothers1.png");
	pawelKarpinskiImage = loadImage("karpinskiBros2.png");
	countessHamiltonImage = loadImage("countessHamilton.png");
	robbieKrayImage = loadImage("krayBrothers2.png");

}

function setup()
{
	createCanvas(photoBoard.width, photoBoard.height);
}

function draw()
{
	image(photoBoard, 0, 0);

	//And update these image commands with your x and y coordinates.
	image(annaKarpinskiImage, annaKarpinskiCoordX, annaKarpinskiCoordY);
	image(cecilKarpinskiImage, cecilKarpinskiCoordX, cecilKarpinskiCoordY);
	image(rockyKrayImage, rockyKrayCoordX, rockyKrayCoordY);
	image(pawelKarpinskiImage, pawelKarpinskiCoordX, pawelKarpinskiCoordY);
	image(countessHamiltonImage, countessHamiltonCoordX, countessHamiltonCoordY);
	image(robbieKrayImage, robbieKrayCoordX, robbieKrayCoordY);

	//image(cecilKarpinskiImage, 115, 40);
	//image(rockyKrayImage, 701, 40);
	//image(pawelKarpinskiImage, 115, 309);
	//image(countessHamiltonImage, 408, 309);
	//image(robbieKrayImage, 701, 309);

}