/*
The case of the Python Syndicate
Stage 3


Officer: 8768770
CaseNum: 301-2-48047861-8768770

Right kid let’s work out which of our ‘friends’ is connected to the syndicate.

- An object for Bones karpinski has been declared and initialised
- Modify the x and y parameters of each image command using the x and y
properties from the Bones karpinski object so the images remain at their correct
positions on the board.
- To do this you will need to combine add and subtract operators with the
relevant property for each parameter



*/

var photoBoard;
var robbieKrayImage;
var countessHamiltonImage;
var cecilKarpinskiImage;
var bonesKarpinskiImage;
var annaKarpinskiImage;
var rockyKrayImage;

var bonesKarpinskiObj;




function preload()
{
	photoBoard = loadImage('photoBoard.png');
	robbieKrayImage = loadImage("krayBrothers2.png");
	countessHamiltonImage = loadImage("countessHamilton.png");
	cecilKarpinskiImage = loadImage("karpinskiBros1.png");
	bonesKarpinskiImage = loadImage("karpinskiDog.png");
	annaKarpinskiImage = loadImage("karpinskiWoman.png");
	rockyKrayImage = loadImage("krayBrothers1.png");

}

function setup()
{
	createCanvas(photoBoard.width, photoBoard.height);
	bonesKarpinskiObj = {
		x: 115,
		y: 309,
		image: bonesKarpinskiImage
	};
}

function draw()
{
	image(photoBoard, 0, 0);

	//And update these image commands with your x and y coordinates.
	image(bonesKarpinskiObj.image, bonesKarpinskiObj.x, bonesKarpinskiObj.y);

	image(robbieKrayImage, bonesKarpinskiObj.x, bonesKarpinskiObj.y - 269);
	image(countessHamiltonImage, bonesKarpinskiObj.x + 293, bonesKarpinskiObj.y - 269);
    image(cecilKarpinskiImage, bonesKarpinskiObj.x + 586, bonesKarpinskiObj.y - 269);
	image(annaKarpinskiImage, bonesKarpinskiObj.x + 293, bonesKarpinskiObj.y);
	image(rockyKrayImage, bonesKarpinskiObj.x + 586, bonesKarpinskiObj.y);

}