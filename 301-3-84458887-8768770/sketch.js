/*
The case of the Python Syndicate
Stage 4

Officer: 8768770
CaseNum: 301-3-84458887-8768770

To really crack the Python Syndicate we need to go in deep. I want to understand
all the connections. I have the data but it’s a mess and I need you to sort it out.

Organise each syndicate member into an object. I’ve done one for you as an example.
Be sure to replicate the naming conventions for your own objects.
Use image command together with the objects you created to organise the mugshots.

- Once you have done this you can comment out or delete the old variables.

*/

var photoBoard;
var robbieKrayImage;
var cecilKarpinskiImage;
var linaLovelaceImage;
var countessHamiltonImage;
var rockyKrayImage;
var pawelKarpinskiImage;

var linaLovelaceObject;


//declare your new objects below


var robbieKrayXCoord = 115;
var robbieKrayYCoord = 40;
var cecilKarpinskiXCoord = 408;
var cecilKarpinskiYCoord = 40;
var countessHamiltonXCoord = 115;
var countessHamiltonYCoord = 309;
var rockyKrayXCoord = 408;
var rockyKrayYCoord = 309;
var pawelKarpinskiXCoord = 701;
var pawelKarpinskiYCoord = 309;


function preload()
{
	photoBoard = loadImage('photoBoard.png');
	robbieKrayImage = loadImage("krayBrothers2.png");
	cecilKarpinskiImage = loadImage("karpinskiBros1.png");
	linaLovelaceImage = loadImage("lina.png");
	countessHamiltonImage = loadImage("countessHamilton.png");
	rockyKrayImage = loadImage("krayBrothers1.png");
	pawelKarpinskiImage = loadImage("karpinskiBros2.png");

}

function setup()
{
	createCanvas(photoBoard.width, photoBoard.height);
	linaLovelaceObject = {
		x: 701,
		y: 40,
		image: linaLovelaceImage
	};



	//define your new objects below
	robbieKrayObject = {
		x: robbieKrayXCoord,
		y: robbieKrayYCoord,
		image: robbieKrayImage
	};
	cecilKarpinskiObject = {
		x: cecilKarpinskiXCoord,
		y: cecilKarpinskiYCoord,
		image: cecilKarpinskiImage
	};
	countessHamiltonObject = {
		x: countessHamiltonXCoord,
		y: countessHamiltonYCoord,
		image: countessHamiltonImage
	};
	rockyKrayObject = {
		x: rockyKrayXCoord,
		y: rockyKrayYCoord,
		image: rockyKrayImage
	};
	pawelKarpinskiObject = {
		x: pawelKarpinskiXCoord,
		y: pawelKarpinskiYCoord,
		image: pawelKarpinskiImage
	};

}

function draw()
{
	image(photoBoard, 0, 0);

	//And update these image commands with your x and y coordinates.
	image(robbieKrayObject.image, robbieKrayObject.x, robbieKrayObject.y);
	image(cecilKarpinskiObject.image, cecilKarpinskiObject.x, cecilKarpinskiObject.y);
	image(linaLovelaceObject.image, linaLovelaceObject.x, linaLovelaceObject.y);
	image(countessHamiltonObject.image, countessHamiltonObject.x, countessHamiltonObject.y);
	image(rockyKrayObject.image, rockyKrayObject.x, rockyKrayObject.y);
	image(pawelKarpinskiObject.image, pawelKarpinskiObject.x, pawelKarpinskiObject.y);


}