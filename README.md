# sleuth-assessment
University of London's Introduction to Computer Programming course thru Coursera. The main assessment - sleuth, uses P5.js and various sketches to test programming ability. 

We start with an empty sketch and modify it to meet the requirements. It's then checked via upload and given a grade. So far only one has been frustratingly incorrect - an outline had to be drawn around a corpse and no matter what I tried the grader rejected it. The rest have gone smoothly, more homework than exam, really.

Each case is broken into parts, and each part only available for download after the earlier parts are completed. Thus, I am (and should have been all along) commiting to mark each part finished with the new files downloaded and placed in the repo.